#!/bin/bash

NAME=ardour-ci-docker-jessie-mingw

# The :Z postfix is to work around issues with selinux

sudo docker run -t --rm -v ~/docker-share/$NAME:/var/tmp:Z $NAME
